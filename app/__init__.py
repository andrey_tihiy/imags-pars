import sys
import os
import json
import spider

class SpiderApp():
    def __init__(self):
        path = 'data.json'
        with open(path, 'r') as f:
            self.data = json.loads(f.read())

    def run_pars(self):
       spider.run_boy_run(self.data)

                
def main():
    app = SpiderApp()
    app.run_pars()
