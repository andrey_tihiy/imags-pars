import scrapy
from scrapy.crawler import CrawlerProcess
from .items import start_record_in_file

data_pars = []
links_page = []
links_single_page = []
record_data = []

class Spider(scrapy.Spider):
    def __init__(self):
        self.allowed_domains = [data_pars[0]['url_web']]
        self.start_urls = links_page
        #self.allowed_domains = ["santechshara.com.ua"]
        #self.start_urls = ["https://www.santechshara.com.ua/santehnika/santehnika-v-chernom-tsvete/page-2/"]
    name = 'imags_spyder'
    def parse(self, response):
        #SET_SELECTOR = response.css(data_pars[0]['catalog_item']['html_element']+'.'+data_pars[0]['catalog_item']['css_class']).css(data_pars[0]['wrapper_link_in_item_catalog']['html_element']+'.'+data_pars[0]['wrapper_link_in_item_catalog']['css_class']).css(data_pars[0]['link_in_item_catalog']['html_element']+'.'+data_pars[0]['link_in_item_catalog']['css_class']+'::attr(href)')
        SET_SELECTOR = response.css('div.selected__item').css('div.describe').css('a.selected__name::attr(href)').getall()
        #SET_SELECTOR = response.xpath('//a[@class="selected__name"]/@href').extract()

        for y in SET_SELECTOR:
            resp = yield scrapy.Request('https://www.santechshara.com.ua'+y, callback=self.parse_content, dont_filter=True)
            record_data.append(resp)


    def parse_content(self, response):
        discript_str = ''
        tech_str = ''
        title =  response.xpath("//h1/text()").get()
        price = response.xpath("//span[@class='current-price']/text()").get()
        photo = "https://www.santechshara.com.ua" + response.xpath("//img[@class='zoomed']/@src").get()
        discript_list = response.xpath("//div[@class='info-block']/div[@class='text']/p").get()
        tech_data = response.xpath("//div[@class='info-block']/div[@class='table']").get()
        data_r = {"Название":response.xpath("//h1/text()").get(), "Фото":"https://www.santechshara.com.ua" + response.xpath("//img[@class='zoomed']/@src").get(), "Цена":response.xpath("//span[@class='current-price']/text()").get(), "Описание":discript_list ,"Технические характеристики": tech_data}
        start_record_in_file(data_r)





process = CrawlerProcess(settings={
    'FEED_FORMAT': 'json',
})
def run_boy_run(data_par):
    data_pars.append(data_par)
    for i in range(int(data_par['number_page_to_pars'])):
        links_page.append(data_par['url_category']+data_par['label_page_num']+str(i+1))
    process.crawl(Spider)
    process.start()








